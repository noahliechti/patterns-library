@import "../grid/grid-settings";
@import "max-width";

/// Get the next breakpoint.
/// @param {string} $breakpoint - Breakpoint to use of $breakpoints map.
/// @param {Map} $breakpoints [$grid-breakpoints] - The grid-tiers and associated breakpoints.
/// @param {list} $breakpoint-names [map-keys($breakpoints)] - All available breakpoint keys.
@function next-breakpoint($breakpoint, $breakpoints: $grid-breakpoints, $breakpoint-names: map-keys($breakpoints)) {
  $n: index($breakpoint-names, $breakpoint);

  @return if($n < length($breakpoint-names), nth($breakpoint-names, $n + 1), null);
}

/// Get a specific breakpoint, either by key or concrete number.
/// @param {string} $breakpoint - Breakpoint to use of $breakpoints map.
/// @param {Map} $breakpoints [$grid-breakpoints] - The grid-tiers and associated breakpoints.
/// @param {up|down} $direction [down] - Direction of the breakpoint.
@function get-breakpoint($breakpoint, $breakpoints: $grid-breakpoints, $direction: down) {
  @if type_of($breakpoints) != map {
    @error "$breakpoints #{$breakpoints} has to be a Map.";
  }

  $type-of-breakpoint: type_of($breakpoint);
  $breakpoint-is-number: $type-of-breakpoint == number;
  $breakpoint-is-mapped: $type-of-breakpoint == string and map_has_key($breakpoints, $breakpoint);

  @if ( not $breakpoint-is-number and not $breakpoint-is-mapped) {
    @error "$breakpoint #{$breakpoint} has to be either a Number or valid key of a breakpoints Map.";
  }

  @if $breakpoint-is-mapped {
    @if $direction == down {
      $breakpoint: next-breakpoint($breakpoint, $breakpoints);
      $breakpoint: max-width(map_get($breakpoints, $breakpoint));
    } @else {
      $breakpoint: map_get($breakpoints, $breakpoint);
    }
  }

  @if ( type_of($breakpoint) == number and $breakpoint < 0 ) {
    @error "$breakpoint #{$breakpoint} can't be negative.";
  }

  @return $breakpoint;
}

/// Minimum breakpoint width. Null for the smallest (first) breakpoint.
///
///    >> breakpoint-min(sm, (xs: 0, sm: 576px, md: 768px, lg: 992px, xl: 1200px))
///    576px
///
/// @param {string} $name - Breakpoint name to use of $breakpoints map.
/// @param {Map} $breakpoints [$grid-breakpoints] - The grid-tiers and associated breakpoints.
@function breakpoint-min($name, $breakpoints: $grid-breakpoints) {
  $min: map-get($breakpoints, $name);

  @return if($min != 0, $min, null);
}

/// Maximum breakpoint width. Null for the largest (last) breakpoint.
/// The maximum value is calculated as the minimum of the next one less 0.02px
/// to work around the limitations of `min-` and `max-` prefixes and viewports with fractional widths.
/// See https://www.w3.org/TR/mediaqueries-4/#mq-min-max
/// Uses 0.02px rather than 0.01px to work around a current rounding bug in Safari.
/// See https://bugs.webkit.org/show_bug.cgi?id=178261
///
///    >> breakpoint-max(sm, (xs: 0, sm: 576px, md: 768px, lg: 992px, xl: 1200px))
///    767.98px
///
/// @param {string} $name - Breakpoint name to use of $breakpoints map.
/// @param {Map} $breakpoints [$grid-breakpoints] - The grid-tiers and associated breakpoints.
@function breakpoint-max($name, $breakpoints: $grid-breakpoints) {
  $next: breakpoint-next($name, $breakpoints);

  @return if($next, breakpoint-min($next, $breakpoints) - 0.02px, null);
}

/// Returns a blank string if smallest breakpoint, otherwise returns the name with a dash in front.
/// Useful for making responsive utilities.
///
///    >> breakpoint-infix(xs, (xs: 0, sm: 576px, md: 768px, lg: 992px, xl: 1200px))
///    ""  (Returns a blank string)
///    >> breakpoint-infix(sm, (xs: 0, sm: 576px, md: 768px, lg: 992px, xl: 1200px))
///    "-sm"
///
/// @param {string} $name - Breakpoint name to use of $breakpoints map.
/// @param {Map} $breakpoints [$grid-breakpoints] - The grid-tiers and associated breakpoints.
@function breakpoint-infix($name, $breakpoints: $grid-breakpoints) {
  @return if(breakpoint-min($name, $breakpoints) == null, "", "-#{$name}");
}
