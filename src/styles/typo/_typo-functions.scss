@import "./typo-settings";
@import "../functions/map";

/// Get the compound key for a specific typeface variation.
/// @link https://design.axa.com/web-guidelines/typography#properties
/// @param {string} $name - The name of a specific typeface variation.
/// @param {string} $face [null] - A valid alternative face, e.g. `publico`.
/// @returns {string} - Returns a valid key for the responsive typeface variation map.
@function get-typo-key($name, $face: null) {
  @if type-of($name) != string {
    @error "$name #{$name} is not of type string";
  }

  $key: #{$name};

  @if $face {
    @if type-of($face) != string {
      @error "$face #{$face} is not of type string";
    }

    $key: #{$key}--#{$face};
  }

  @return $key;
}

/// Get the a specific typeface variation for a specific breakpoint.
/// @link https://design.axa.com/web-guidelines/typography#properties
/// @param {string} $name - The name of a specific typeface variation.
/// @param {string} $breakpoint - The name of a specific breakpoint.
/// @param {string} $face [null] - A valid alternative face, e.g. `publico`.
/// @returns {Map} - Returns a valid responsive typeface variation map.
@function get-typo($name, $breakpoint: sm, $face: null) {
  @if type-of($breakpoint) != string {
    @error "$breakpoint #{$breakpoint} is not of type string";
  }

  @if not map-has-key($typo-map, $breakpoint) {
    @error "Breakpoint #{$breakpoint} not found in $typo-map.";
  }

  $typos: map_get($typo-map, $breakpoint);
  $key: get-typo-key($name, $face);

  @if not map-has-key($typos, $key) {
    @error "Typo #{$key} @ #{$breakpoint} not found in $typo-map.";
  }

  @return map-get($typos, $key);
}

/// Get the a specific typeface variation for before a specific breakpoint.
/// This is used to determine if font properties are redundant and hence can be skipped.
/// @link https://design.axa.com/web-guidelines/typography#properties
/// @param {string} $name - The name of a specific typeface variation.
/// @param {string} $breakpoint - The name of a specific breakpoint.
/// @param {string} $face [null] - A valid alternative face, e.g. `publico`.
/// @returns {Map|null} - Returns a valid responsive typeface variation map or `null` if none exists before.
@function get-typo-before($name, $breakpoint: sm, $face: null) {
  @if type-of($breakpoint) != string {
    @error "$breakpoint #{$breakpoint} is not of type string";
  }

  $typos: map-previous-value($typo-map, $breakpoint);

  @if type_of($typos) == map {
    $key: get-typo-key($name, $face);

    @return map-get($typos, $key);
  }

  @return null;
}

/// Get a specific property of a specific typeface variation for a specific breakpoint.
/// @param {Map} $typo - The name of a specific typeface variation.
/// @param {string} $key - The name of a specific typeface property.
/// @returns {any} - Returns the specific typeface property of a specific breakpoint.
@function typo-prop($typo, $key) {
  @if not map-has-key($typo, $key) {
    @error "Key #{$key} not found in $typo-";
  }

  @return map-get($typo, $key);
}

/// Get the font-face by name.
/// @param {string} $face [null] - A valid alternative face, e.g. `publico`.
/// @returns {string|null} - Returns a valid CSS font-family property value or null.
@function get-face($face: null) {
  @if not map-has-key($face-map, $face) {
    @return null;
  }

  @return map-get($face-map, $face);
}

/// Get a specific font weight's name corresponding CSS `font-weight` number.
/// @link https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight#Common_weight_name_mapping
/// @link here: http://www.w3.org/TR/css3-fonts/#font-weight-numeric-values
/// @param {Map} $typo - The name of a specific typeface variation.
/// @param {string} $weight [null] - The name of a font weight.
/// @returns {number} - Returns a valid CSS `font-weight` value.
@function get-weight($typo, $weight: null) {
  $allowedWeights: typo-prop($typo, weight);

  @if not $weight {
    $weight: nth($allowedWeights, 1);
  }

  @if not map-has-key($font-weight-map, $weight) {
    @error "$weight #{$weight} not found in $font-weight-map.";
  }

  @return map-get($font-weight-map, $weight);
}
