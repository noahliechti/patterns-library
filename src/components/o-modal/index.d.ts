export interface Props {
  open: boolean;
}

export default interface AXAModal extends HTMLElement, Props {}
