export interface Props {
  //todo: specify the props
  [key: string]: any
}

export default interface AXAHeaderNavigation extends HTMLElement, Props {
}
