export interface Props {
  //todo: specify the props
  [key: string]: any
}

export default interface AXAFooter extends HTMLElement, Props {
}
