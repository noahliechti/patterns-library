export interface Props {
  //todo: specify the props
  [key: string]: any
}

export default interface AXAPolicyFeatureItem extends HTMLElement, Props {
}
