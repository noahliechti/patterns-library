export interface Props {
  //todo: specify the props, or remove the below line if component has no props 
  [key:string]: any
}

export default interface AXAFormOptionBox extends HTMLElement, Props {
}