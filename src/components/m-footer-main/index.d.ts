export interface Props {
  //todo: specify the props
  [key: string]: any
}

export default interface AXAFooterMain extends HTMLElement, Props {
}
