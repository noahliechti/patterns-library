export interface Props {
  //todo: specify the props
  [key: string]: any
}

export default interface AXAHeaderMobile extends HTMLElement, Props {
}
