import styles from './index.scss';
import BaseComponentGlobal from '../../js/abstract/base-component-global';

BaseComponentGlobal.appendGlobalStyles(styles);
