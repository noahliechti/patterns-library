export interface Props {
  //todo: specify the props
  [key: string]: any
}

export default interface AXAHeaderBurger extends HTMLElement, Props {
}
