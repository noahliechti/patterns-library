import PropTypes from './prop-types';

const floatPropType = PropTypes.oneOf([
  'left',
  'right',
]);

export default floatPropType;
