import PropTypes from './prop-types';

const valuePropType = PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.number,
]);

export default valuePropType;
