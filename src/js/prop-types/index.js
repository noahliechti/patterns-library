import PropTypes from './prop-types';

export { default as alignPropType } from './align-prop-type';
export { default as floatPropType } from './float-prop-type';
export { default as localePropType } from './locale-prop-type';
export { default as sidePropType } from './side-prop-type';
export { default as sortPropType } from './sort-prop-type';
export { default as statePropType } from './state-prop-type';
export { default as tableScopePropType } from './table-scope-prop-type';
export * from './table-row-prop-types';
export * from './table-cell-prop-types';
export * from './table-prop-types';
export { default as urlPropType } from './url-prop-type';
export { default as valuePropType } from './value-prop-type';

export default PropTypes;
