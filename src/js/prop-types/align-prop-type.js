import PropTypes from './prop-types';

const alignPropType = PropTypes.oneOf([
  'left',
  'center',
  'right',
]);

export default alignPropType;
