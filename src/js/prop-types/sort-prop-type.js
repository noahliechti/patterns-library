import PropTypes from './prop-types';

const sortPropType = PropTypes.oneOf([
  'up',
  'down',
]);

export default sortPropType;
