import PropTypes from './prop-types';

const sidePropType = PropTypes.oneOf([
  'top',
  'bottom',
]);

export default sidePropType;
