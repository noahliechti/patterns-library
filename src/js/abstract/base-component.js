import { withBaseAndAllHocs } from './hocs';

export default withBaseAndAllHocs(HTMLElement);
