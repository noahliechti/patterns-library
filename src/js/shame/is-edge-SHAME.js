const isEdge = window.navigator.userAgent.indexOf('Edge') > -1;

export default isEdge;
