/* eslint-disable */
import './es6-polyfills';
import 'document-register-element';
// load this for browsers which support customElements without builtin (webkit)
import '@ungap/custom-elements-builtin';

// First import the withReact integration helper function
import withReact from './with-react';

// re-export all wrapped custom elements
/* eslint-disable import/first, import/newline-after-import */
import AXACheckboxWC from '../components/a-checkbox/';
export const AXACheckbox = withReact(AXACheckboxWC);

import AXAChoiceWC from '../components/a-choice/';
export const AXAChoice = withReact(AXAChoiceWC);

import AXAIconWC from '../components/a-icon/';
export const AXAIcon = withReact(AXAIconWC);

import AXAInputWC from '../components/a-input/';
export const AXAInput = withReact(AXAInputWC);

import AXARadioWC from '../components/a-radio/';
export const AXARadio = withReact(AXARadioWC);

import AXATypoWC from '../components/a-typo/';
export const AXATypo = withReact(AXATypoWC);

import AXAVerticalRhythmWC from '../components/a-vertical-rhythm/';
export const AXAVerticalRhythm = withReact(AXAVerticalRhythmWC);

import AXAAccordionItemWC from '../components/m-accordion-item/';
export const AXAAccordionItem = withReact(AXAAccordionItemWC);

import AXAButtonWC from '../components/m-button/';
export const AXAButton = withReact(AXAButtonWC);

import AXAColWC from '../components/m-col/';
export const AXACol = withReact(AXAColWC);

// @todo: datepicker is used twice
import AXAMDatepickerWC from '../components/m-datepicker/';
export const AXAMDatepicker = withReact(AXAMDatepickerWC);

import AXADatepickerBodyWC from '../components/m-datepicker-body/';
export const AXADatepickerBody = withReact(AXADatepickerBodyWC);

import AXADropdownWC from '../components/m-dropdown/';
export const AXADropdown = withReact(AXADropdownWC);

import AXAFooterLanguagesWC from '../components/m-footer-languages/';
export const AXAFooterLanguages = withReact(AXAFooterLanguagesWC);

import AXAFooterLegalsWC from '../components/m-footer-legals/';
export const AXAFooterLegals = withReact(AXAFooterLegalsWC);

import AXAFooterLinksWC from '../components/m-footer-links/';
export const AXAFooterLinks = withReact(AXAFooterLinksWC);

import AXAFooterMainWC from '../components/m-footer-main/';
export const AXAFooterMain = withReact(AXAFooterMainWC);

import AXAFooterSocialWC from '../components/m-footer-social/';
export const AXAFooterSocial = withReact(AXAFooterSocialWC);

import AXAFooterSubWC from '../components/m-footer-sub/';
export const AXAFooterSub = withReact(AXAFooterSubWC);

import AXAFormGroupWC from '../components/m-form-group/';
export const AXAFormGroup = withReact(AXAFormGroupWC);

import AXAHeaderBurgerWC from '../components/m-header-burger/';
export const AXAHeaderBurger = withReact(AXAHeaderBurgerWC);

import AXAHeaderLanguagesWC from '../components/m-header-languages/';
export const AXAHeaderLanguages = withReact(AXAHeaderLanguagesWC);

import AXAHeaderLogoWC from '../components/m-header-logo/';
export const AXAHeaderLogo = withReact(AXAHeaderLogoWC);

import AXAHeaderMainWC from '../components/m-header-main/';
export const AXAHeaderMain = withReact(AXAHeaderMainWC);

import AXAHeaderMetaWC from '../components/m-header-meta/';
export const AXAHeaderMeta = withReact(AXAHeaderMetaWC);

import AXAHeaderMetaRightWC from '../components/m-header-meta-right/';
export const AXAHeaderMetaRight = withReact(AXAHeaderMetaRightWC);

import AXAHeaderMobileWC from '../components/m-header-mobile/';
export const AXAHeaderMobile = withReact(AXAHeaderMobileWC);

import AXAHeaderMobileLanguagesWC from '../components/m-header-mobile-languages/';
export const AXAHeaderMobileLanguages = withReact(AXAHeaderMobileLanguagesWC);

import AXAHeaderMobileNavigationWC from '../components/m-header-mobile-navigation/';
export const AXAHeaderMobileNavigation = withReact(AXAHeaderMobileNavigationWC);

import AXAHeaderMobileOthersWC from '../components/m-header-mobile-others/';
export const AXAHeaderMobileOthers = withReact(AXAHeaderMobileOthersWC);

import AXAHeaderNavigationWC from '../components/m-header-navigation/';
export const AXAHeaderNavigation = withReact(AXAHeaderNavigationWC);

import AXAHeaderOthersWC from '../components/m-header-others/';
export const AXAHeaderOthers = withReact(AXAHeaderOthersWC);

import AXAHeaderSearchWC from '../components/m-header-search/';
export const AXAHeaderSearch = withReact(AXAHeaderSearchWC);

import AXAHeaderSubNavigationWC from '../components/m-header-sub-navigation/';
export const AXAHeaderSubNavigation = withReact(AXAHeaderSubNavigationWC);

import AXAHeaderTopContentBarWC from '../components/m-header-top-content-bar/';
export const AXAHeaderTopContentBar = withReact(AXAHeaderTopContentBarWC);

import AXALinkWC from '../components/m-link/';
export const AXALink = withReact(AXALinkWC);

import AXAPolicyFeatureItemWC from '../components/m-policy-feature-item/';
export const AXAPolicyFeatureItem = withReact(AXAPolicyFeatureItemWC);

import AXARowWC from '../components/m-row/';
export const AXARow = withReact(AXARowWC);

import AXAAccordionWC from '../components/o-accordion/';
export const AXAAccordion = withReact(AXAAccordionWC);

import AXACommercialHeroCoverWC from '../components/o-commercial-hero-cover/';
export const AXACommercialHeroCover = withReact(AXACommercialHeroCoverWC);

import AXAContainerWC from '../components/o-container/';
export const AXAContainer = withReact(AXAContainerWC);

import AXACookieDisclaimerWC from '../components/o-cookie-disclaimer/';
export const AXACookieDisclaimer = withReact(AXACookieDisclaimerWC);

import AXADatepickerWC from '../components/o-datepicker/';
export const AXADatepicker = withReact(AXADatepickerWC);

import AXAErrorPageWC from '../components/o-error-page/';
export const AXAErrorPage = withReact(AXAErrorPageWC);

import AXAFooterWC from '../components/o-footer/';
export const AXAFooter = withReact(AXAFooterWC);

import AXAHeaderWC from '../components/o-header/';
export const AXAHeader = withReact(AXAHeaderWC);

import AXAPolicyFeaturesWC from '../components/o-policy-features/';
export const AXAPolicyFeatures = withReact(AXAPolicyFeaturesWC);

import AXAStickyWC from '../components/m-sticky/';
export const AXASticky = withReact(AXAStickyWC);

import AXAStickyContainerWC from '../components/o-sticky-container/';
export const AXAStickyContainer = withReact(AXAStickyContainerWC);

import AXATestimonialsWC from '../components/o-testimonials/';
export const AXATestimonials = withReact(AXATestimonialsWC);

import AXATableWC from '../components/o-table/';
export const AXATable = withReact(AXATableWC);

import AXATheadWC from '../components/m-thead/';
export const AXAThead = withReact(AXATheadWC);

import AXATbodyWC from '../components/m-tbody/';
export const AXATbody = withReact(AXATbodyWC);

import AXATfootWC from '../components/m-tfoot/';
export const AXATfoot = withReact(AXATfootWC);

import AXATdWC from '../components/a-td/';
export const AXATd = withReact(AXATdWC);

import AXACaptionWC from '../components/a-caption/';
export const AXACaption = withReact(AXACaptionWC);

import AXATrWC from '../components/m-tr/';
export const AXATr = withReact(AXATrWC);

import AXAThWC from '../components/a-th/';
export const AXATh = withReact(AXAThWC);

import AXACardWC from '../components/o-card';
export const AXACard = withReact(AXACardWC);

import AXAFormOptionBoxWC from '../components/o-form-option-box';
export const AXAFormOptionBox = withReact(AXAFormOptionBoxWC);

import AXAModalWC from '../components/o-modal/';
export const AXAModal = withReact(AXAModalWC);

import AXAProgressBarWC from '../components/o-progress-bar/';
export const AXAProgressBar = withReact(AXAProgressBarWC);
